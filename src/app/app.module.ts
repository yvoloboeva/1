import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrivetstvieComponent } from './components/privetstvie/privetstvie.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ResultComponent } from './components/result/result.component';
import { WorkoutComponent } from './components/workout/workout.component';


const routes: Routes =  [  { path: '', component: PrivetstvieComponent },
{ path: 'training', component:WorkoutComponent},
{ path: 'results', component: ResultComponent }, ];

@NgModule({
  declarations: [
    AppComponent,
    PrivetstvieComponent,
    WorkoutComponent,
    ResultComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ButtonModule,
    AppRoutingModule

  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
