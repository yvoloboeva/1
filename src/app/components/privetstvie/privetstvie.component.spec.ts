import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivetstvieComponent } from './privetstvie.component';

describe('PrivetstvieComponent', () => {
  let component: PrivetstvieComponent;
  let fixture: ComponentFixture<PrivetstvieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivetstvieComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivetstvieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
