import { PrimeNGConfig } from 'primeng/api';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { Title } from '@angular/platform-browser';
import { UserService } from './user.service';
import { BsModalService} from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-privetstvie',
  templateUrl: './privetstvie.component.html',
  styleUrls: ['./privetstvie.component.css'],
  template: `<h1>Hello {{ username }}!</h1> `
 
})

export class PrivetstvieComponent implements  OnInit {
   
  username: string | undefined;
  userInput: string = '';
   submitted: boolean = false;
   showButtons = false;
   showLinks = false;

     constructor(private primengConfig: PrimeNGConfig,private router: Router, private titleService: Title, private userService: UserService, private modalService: BsModalService,public bsModalRef: BsModalRef) {} 
    ngOnInit() {
    this.primengConfig.ripple = true; 
    this.userService.setUsername(this.userInput);
    this.userInput = '';

  }
  nSelect(){ 
    this.submitted = true;
    this.titleService.setTitle(`${this.userInput}`)
    this.showButtons = true;
    this.showLinks = true;


  }
  startTraining(){
    this.router.navigate(['/training']);

  }
  showResults(){
    this.router.navigate(['/results']);
  }
 

}
