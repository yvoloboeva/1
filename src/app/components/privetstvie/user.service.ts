import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  public name = ' ';
  setUsername(name: string) {
    this.name = name;
  }

  getUsername() {
    return this.name;
  }
  constructor() { }
}
