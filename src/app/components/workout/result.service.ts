import { Injectable } from '@angular/core';

interface Result {
  startTime: Date;
  endTime: Date;
  isCorrect: boolean;
}


@Injectable({
  providedIn: 'root'
})

export class ResultService {
  private results: Result[] = [];

  addResult(startTime: Date, endTime: Date, isCorrect: boolean) {
    this.results.push({ startTime, endTime, isCorrect });
  }

  finishTraining() {
    console.log(this.results);
  }
  
  constructor() { }
}
